from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self):
        return "Request has been added"

    def getFirstName(self):
        return "Your first name is " + self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return "Your first name is " + self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        print(f"Your email is {self._email}")

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return "Your department is " + self._department

    def setDepartment(self, department):
        self._department = department

    def checkRequest(self):
        print(f"Request has been checked!")

    def addUser(self):
        print(f"User Added")

    def login(self):
        return self._email + " has logged in"

    def logout(self):
        return self._email + " has logged out"


class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self.member_list = []

    def addRequest(self):
        return "Request has been added"

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self):
        return "Request has been added"

    def getFirstName(self):
        return "Your first name is " + self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return "Your first name is " + self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        print(f"Your email is {self._email}")

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return "Your department is " + self._department

    def setDepartment(self, department):
        self._department = department

    def checkRequest(self):
        print(f"Request has been checked!")

    def addUser(self):
        print(f"User Added")

    def login(self):
        return self._email + " has logged in"

    def logout(self):
        return self._email + " has logged out"

    def addMember(self, Employee):
        self.member_list.append(Employee)

    def get_members(self):
        return self.member_list


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def addRequest(self):
        return "Request has been added"

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self):
        return "Request has been added"

    def getFirstName(self):
        return "Your first name is " + self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return "Your first name is " + self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        print(f"Your email is {self._email}")

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return "Your department is " + self._department

    def setDepartment(self, department):
        self._department = department

    def checkRequest(self):
        print(f"Request has been checked!")

    def addUser(self):
        print(f"User Added")

    def login(self):
        return self._email + " has logged in"

    def logout(self):
        return self._email + " has logged out"

class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested

    def getName(self):
        print(f"Your name is {self._firstName}")

    def setName(self, name):
        self._name = name

    def getRequester(self):
        print(f"The requester is {self._requester}")

    def setRequester(self, requester):
        self._requester = requester

    def getDateRequested(self):
        print(f"Your date requested is {self._dateRequested}")

    def setDateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def getStatus(self):
        print(f"Your status is {self._status}")

    def set_status(self, status):
        self._status = status

    def updateRequest(self):
        return "Request " +  self._name + " has been updated"

    def closeRequest(self):
        return "Request " +  self._name + " has been closed"

    def cancelRequest(self):
        return "Request " +  self._name + " has been cancelled"



# Test cases
employee1 = Employee("John", "Doe", "djohn@gmail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@gmail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@gmail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@gmail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)


for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())


req2.set_status("closed")
print(req2.closeRequest())
